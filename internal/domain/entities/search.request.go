package entities

type SearchRequest struct {
	Text string `json:"text"`
}
