package entities

type SearchResponse struct {
	Number string `json:"number"`
	Value  int    `json:"value"`
}
