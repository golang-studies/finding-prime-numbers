package services

import (
	"net/http"

	"gitlab.com/golang-studies/finding-prime-numbers/internal/application/enums"
	"gitlab.com/golang-studies/finding-prime-numbers/internal/domain/entities"
	"gitlab.com/golang-studies/finding-prime-numbers/internal/integration/utils"
)

type SearchService struct {
}

func NewSearchService() *SearchService {
	return &SearchService{}
}

func (service *SearchService) SearchSmallestPrimeNumber(text string) (*entities.SearchResponse, *entities.ErrorResponse) {

	romanNumbers := utils.GetRomanNumbers(text)
	if len(romanNumbers) < 1 {
		return nil, entities.NewErrorResponse(http.StatusBadRequest, enums.ERROR_LOG_TEXT_WITHOUT_ROMAN_NUMBERS, "Text without roman numerals")
	}

	numbers, err := utils.ListRomanNumberToListNumberInteger(romanNumbers)
	if err != nil {
		return nil, entities.NewErrorResponse(http.StatusBadRequest, enums.ERROR_LOG_INVALID_ROMAN_NUMBERS, err.Error())
	}

	number := utils.GetSmallestPrimeNumber(numbers)
	if number < 1 {
		return nil, entities.NewErrorResponse(http.StatusBadRequest, enums.ERROR_LOG_DOESNT_HAVE_PRIME_NUMBERS, "Doesn't have prime numbers")
	}

	return &entities.SearchResponse{Number: numbers[number], Value: number}, nil
}
