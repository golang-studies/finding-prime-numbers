package services

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"reflect"
	"testing"

	"gitlab.com/golang-studies/finding-prime-numbers/internal/domain/entities"
)

type searchSmallestPrimeNumber struct {
	Text     string                   `json:"text"`
	Response *entities.SearchResponse `json:"response"`
}

func TestSearchSmallestPrimeNumber(t *testing.T) {
	jsonFile, err := os.Open("../../../test/resource/search-smallest-prime-number.json")
	if err != nil {
		t.Errorf("failed to open json file | %s", err.Error())
	}
	byteValue, _ := ioutil.ReadAll(jsonFile)

	var search *searchSmallestPrimeNumber
	json.Unmarshal([]byte(byteValue), &search)

	service := NewSearchService()
	response, errResponse := service.SearchSmallestPrimeNumber(search.Text)

	if errResponse != nil {
		t.Errorf("failed to open json file | %s", errResponse.Message)
	}

	if !reflect.DeepEqual(search.Response, response) {
		t.Errorf("falied responses not equals | ExpectedResponse{number=%s, value=%d} / Response{number=%s, value=%d}", search.Response.Number, search.Response.Value, response.Number, response.Value)
	}
}
