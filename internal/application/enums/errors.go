package enums

type ErrorLog string

const (
	ERROR_LOG_TEXT_WITHOUT_ROMAN_NUMBERS = "ERROR_LOG_TEXT_WITHOUT_ROMAN_NUMBERS"
	ERROR_LOG_INVALID_ROMAN_NUMBERS      = "ERROR_LOG_INVALID_ROMAN_NUMBERS"
	ERROR_LOG_DOESNT_HAVE_PRIME_NUMBERS  = "ERROR_LOG_DOESNT_HAVE_PRIME_NUMBERS"
)
