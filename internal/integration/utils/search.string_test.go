package utils

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"reflect"
	"testing"
)

type romanNumberToInteger struct {
	RomanNumber []string       `json:"romanNumber"`
	Integer     map[int]string `json:"integer"`
}

type getSmallestPrimeNumber struct {
	Numbers  map[int]string `json:"numbers"`
	Response int            `json:"response"`
}

func TestGetRomanNumbers(t *testing.T) {
	jsonFile, err := os.Open("../../../test/resource/text-to-roman-number.json")
	if err != nil {
		t.Errorf("failed to open json file | %s", err.Error())
	}
	byteValue, _ := ioutil.ReadAll(jsonFile)

	var texts map[string][]string
	json.Unmarshal([]byte(byteValue), &texts)

	for text, romanNumber := range texts {
		romanNumberResponse := GetRomanNumbers(text)
		if !reflect.DeepEqual(romanNumber, romanNumberResponse) {
			t.Errorf("falied getRomanNumbers text>%s | rn>%s>%s", text, romanNumber, romanNumberResponse)
		}
	}
}

func TestListRomanNumberToListNumberInteger(t *testing.T) {
	jsonFile, err := os.Open("../../../test/resource/list-roman-number-to-integer.json")
	if err != nil {
		t.Errorf("failed to open json file | %s", err.Error())
	}
	byteValue, _ := ioutil.ReadAll(jsonFile)

	var numbers []*romanNumberToInteger
	json.Unmarshal([]byte(byteValue), &numbers)

	for _, romanNumberToInteger := range numbers {
		romanNumber := romanNumberToInteger.RomanNumber
		integer := romanNumberToInteger.Integer
		integerResponse, err := ListRomanNumberToListNumberInteger(romanNumber)
		if err != nil {
			t.Errorf("failed to convert list | %s", err.Error())
		}

		if !reflect.DeepEqual(integer, integerResponse) {
			t.Errorf("failed to convert list not equals ")
		}
	}
}

func TestRomanNumberToNumberInteger(t *testing.T) {
	jsonFile, err := os.Open("../../../test/resource/roman-number-to-integer.json")
	if err != nil {
		t.Errorf("failed to open json file | %s", err.Error())
	}
	byteValue, _ := ioutil.ReadAll(jsonFile)

	var numbers map[string]int
	json.Unmarshal([]byte(byteValue), &numbers)

	for romanNumber, integer := range numbers {
		integerResponse := romanNumberToNumberInteger(romanNumber)

		if integerResponse != integer {
			t.Errorf("failed to convert roman number to integer")
		}
	}
}

func TestValidRomanNumber(t *testing.T) {
	jsonFile, err := os.Open("../../../test/resource/roman-number-is-valid.json")
	if err != nil {
		t.Errorf("failed to open json file | %s", err.Error())
	}
	byteValue, _ := ioutil.ReadAll(jsonFile)

	var romanNumbers map[string]bool
	json.Unmarshal([]byte(byteValue), &romanNumbers)

	for romanNumber, isValid := range romanNumbers {
		isValidResponse := validRomanNumber(romanNumber)
		if isValidResponse != isValid {
			t.Errorf("failed to valid [%s][%t<>%t]", romanNumber, isValid, isValidResponse)
		}
	}
}

func TestGetSmallestPrimeNumber(t *testing.T) {
	jsonFile, err := os.Open("../../../test/resource/get-smallest-prime-number.json")
	if err != nil {
		t.Errorf("failed to open json file | %s", err.Error())
	}
	byteValue, _ := ioutil.ReadAll(jsonFile)

	var numbers *getSmallestPrimeNumber
	json.Unmarshal([]byte(byteValue), &numbers)

	response := GetSmallestPrimeNumber(numbers.Numbers)
	if response != numbers.Response {
		t.Errorf("failed to GetSmallestPrimeNumber | %d<>%d", response, numbers.Response)
	}
}

func TestIsPrimeNumber(t *testing.T) {
	jsonFile, err := os.Open("../../../test/resource/valid-is-prime-number.json")
	if err != nil {
		t.Errorf("failed to open json file | %s", err.Error())
	}
	byteValue, _ := ioutil.ReadAll(jsonFile)

	var numbers map[int]bool
	json.Unmarshal([]byte(byteValue), &numbers)

	for number, isPrime := range numbers {
		isPrimeResponse := isPrimeNumber(number)
		if isPrimeResponse != isPrime {
			t.Errorf("failed to valid [%d][%t<>%t]", number, isPrime, isPrimeResponse)
		}
	}
}
