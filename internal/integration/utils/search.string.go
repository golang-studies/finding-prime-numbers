package utils

import (
	"errors"
	"fmt"
	"sort"
	"strings"
)

var (
	RomanNumbers = map[string]int{"I": 1, "V": 5, "X": 10, "L": 50, "C": 100, "D": 500, "M": 1000}
)

func GetRomanNumbers(text string) []string {
	slcText := strings.Split(text, "")
	romanNumbersText := make([]string, 0)

	isRomanNumber := false
	romanNumberTemp := ""
	for _, char := range slcText {
		if RomanNumbers[strings.ToUpper(char)] > 0 {
			romanNumberTemp = fmt.Sprintf("%s%s", romanNumberTemp, strings.ToUpper(char))
			isRomanNumber = true
		} else {
			if isRomanNumber {
				if len(romanNumberTemp) > 0 {
					romanNumbersText = append(romanNumbersText, romanNumberTemp)
				}
				romanNumberTemp = ""
			}
		}
	}
	if len(romanNumberTemp) > 0 {
		romanNumbersText = append(romanNumbersText, romanNumberTemp)
	}
	return romanNumbersText
}

func ListRomanNumberToListNumberInteger(romanNumbers []string) (map[int]string, error) {
	numbers := make(map[int]string, 0)

	for _, rn := range romanNumbers {
		if !validRomanNumber(rn) {
			return nil, errors.New(fmt.Sprintf("Invalid Roman number: %s", rn))
		}

		number := romanNumberToNumberInteger(rn)
		if number < 0 {
			return nil, errors.New(fmt.Sprintf("Invalid Roman number 2: %s", rn))
		}
		numbers[number] = rn
	}
	return numbers, nil
}

func romanNumberToNumberInteger(romanNumber string) int {
	number := 0
	slcRomanNumber := strings.Split(romanNumber, "")
	if len(slcRomanNumber) > 1 {

		for i := 0; i < len(slcRomanNumber); i += 2 {
			sumnumber := 0
			rnumber1 := RomanNumbers[slcRomanNumber[i]]
			rnumber2 := RomanNumbers[slcRomanNumber[i+1]]

			if len(slcRomanNumber)%2 != 0 && i+2 == len(slcRomanNumber)-1 {
				rnumber3 := RomanNumbers[slcRomanNumber[i+2]]

				if (rnumber1 < rnumber2 && rnumber2 < rnumber3) || (rnumber1 == rnumber2 && rnumber2 < rnumber3) {
					return -1
				}

				if rnumber1 < rnumber2 {
					sumnumber += (rnumber2 - rnumber1) + rnumber3
				} else if rnumber2 < rnumber3 {
					sumnumber += rnumber1 + (rnumber3 - rnumber2)
				} else {
					sumnumber += rnumber1 + rnumber2 + rnumber3
				}

				number += sumnumber
				break
			} else {
				if rnumber1 < rnumber2 {
					sumnumber += rnumber2 - rnumber1
				} else {
					sumnumber += rnumber1 + rnumber2
				}
			}
			number += sumnumber
		}
	} else {
		number, _ = RomanNumbers[romanNumber]
	}
	return number
}

func validRomanNumber(romanNumber string) bool {
	slcRomanNumber := strings.Split(romanNumber, "")
	countDuplicate := make(map[string]int, 0)
	for i := 0; i < len(slcRomanNumber); i++ {
		number := slcRomanNumber[i]
		countDuplicate[number] += 1

		if number == "V" && i+1 < len(slcRomanNumber) {
			rnumber1 := RomanNumbers[number]
			rnumber2 := RomanNumbers[slcRomanNumber[i+1]]
			if rnumber1 < rnumber2 {
				return false
			}
		}

		if len(slcRomanNumber)%2 != 0 && i+2 == len(slcRomanNumber)-1 {
			rnumber1 := RomanNumbers[number]
			rnumber2 := RomanNumbers[slcRomanNumber[i+1]]
			rnumber3 := RomanNumbers[slcRomanNumber[i+2]]
			if (rnumber1 < rnumber2 && rnumber2 < rnumber3) || (rnumber1 == rnumber2 && rnumber2 < rnumber3) {
				return false
			}

		}
	}
	for _, duplations := range countDuplicate {
		if duplations > 3 {
			return false
		}
	}
	return true
}

func GetSmallestPrimeNumber(numbers map[int]string) int {
	primeNumber := make([]int, 0)
	for number := range numbers {
		if isPrimeNumber(number) {
			primeNumber = append(primeNumber, number)
		}
	}

	sort.Ints(primeNumber)
	if len(primeNumber) < 1 {
		return -1
	}

	return primeNumber[0]
}

func isPrimeNumber(number int) bool {

	if number <= 1 {
		return false
	}

	if number <= 3 {
		return true
	}

	if number%2 == 0 || number%3 == 0 {
		return false
	}

	for i := 5; i*i <= number; i = i + 6 {
		if number%i == 0 || number%(i+2) == 0 {
			return false
		}
	}
	return true
}
