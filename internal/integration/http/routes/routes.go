package routes

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/golang-studies/finding-prime-numbers/internal/integration/http/controllers"
)

const ASSIGMENT_FINDING_PRIME_NUMVER = "/search"

func SetupAssigmentsRoutes(router fiber.Router, controller *controllers.AssigmentsControllers) {
	router.Post(ASSIGMENT_FINDING_PRIME_NUMVER, controller.FindingSmallestPrimeNumber)
}
