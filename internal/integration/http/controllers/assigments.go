package controllers

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/golang-studies/finding-prime-numbers/internal/application/services"
	"gitlab.com/golang-studies/finding-prime-numbers/internal/domain/entities"
)

type AssigmentsControllers struct {
	search *services.SearchService
}

func NewAssigmentsControllers() *AssigmentsControllers {
	return &AssigmentsControllers{services.NewSearchService()}
}

// FindingSmallestPrimeNumber godoc
// @Summary Get smallest prime number
// @Description Get smallest prime number from text
// @Accept  json
// @Produce  json
// @Param body body entities.SearchRequest true "The text to finding smallest prime number"
// @Success 200 {object} entities.SearchResponse
// @Failure 400 {object} entities.ErrorResponse
// @Failure 500 {object} entities.ErrorResponse
// @Router /search [post]
func (controller *AssigmentsControllers) FindingSmallestPrimeNumber(ctx *fiber.Ctx) error {
	search := new(entities.SearchRequest)

	err := ctx.BodyParser(&search)
	if err != nil {
		return fiber.NewError(fiber.StatusBadRequest, err.Error())
	}

	searchResponse, errResponse := controller.search.SearchSmallestPrimeNumber(search.Text)
	if errResponse != nil {
		return ctx.Status(errResponse.Status).JSON(errResponse)
	}
	return ctx.JSON(searchResponse)
}
