FROM golang:1.18 as build

WORKDIR /app

COPY . .

RUN go mod download

RUN go build -o finding-prime-number ./cmd/main.go

FROM alpine:latest

COPY --from=build /app/finding-prime-number /app/finding-prime-number
COPY --from=build /app/.env /app/.env

RUN chmod +x app/finding-prime-number

CMD ["./app/finding-prime-number"]